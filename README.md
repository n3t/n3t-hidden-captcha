n3t Hidden Captcha
==================

n3t Hidden Captcha plugin for Joomla! adds hidden field to any protected form.
Humans will not be able to see the field, so will not fill it. On the oher hand bots
will try to fill every field in the form. If anything is filled in the field,
form submission is rejected.
