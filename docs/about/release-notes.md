Release notes
=============

3.1.x
-----

#### 3.1.0

- __Final release__ - n3t Hidden Captcha was deprecated in favour of n3t Multi Captcha plugin
- add notification about deprecation to installer and config

3.0.x
-----

#### 3.0.5

- correct rendering of multiple captcha fields on one page

#### 3.0.4

- Chrome ignored autocomplete off, and fills the hidden field in some cases

#### 3.0.3

- Joomla 3.9 and later enables to hide CAPTCHA field including label

#### 3.0.2

- help site included

#### 3.0.1

- logging of fail checks
- field is hidden using external CSS
- description displayed as plain text

#### 3.0.0

- Initial release
