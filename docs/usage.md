Usage
=====

Installation
------------

n3t Hidden Captcha could be installed as any other extension in Joomla! For detailed
information see Joomla documentation.

Do not forget to __enable the plugin__ after installation. 

Joomla! versions
----------------

Currently only Joomla 3.x and newer is supported.

More information could be found in [Release notes](about/release-notes.md)

Enabling Captcha
----------------

To enable n3t Hidden Captcha (or any other captcha in Joomla!) there are few options:

### Global configuration

In Joomla! global configuration there is option to select Default Captcha. If you
select n3t Hidden Captcha here, it shgould be enabled globally for any components
or extensions supporting Captcha, if in that extension is not Captcha disabled,
or selected different Captcha engine.

### Extension based

Every Captcha supporting extension (usually component, but could be module or
plugin also) should have in its configuration field to select Captcha. There is
no general rule, where this field should be, you have to check configuration of
your extension. For example for base Contacts component, you can find field "Allow
Captcha on Contact" on Form page in its configuration.

In some extension this settings could be found not in its configuration but in
parameters of its categories, items etc.

Note that in most cases you will propably enable Captcha in global configuration
and you do not need to take care of extension specific Captcha. This will be propably
used only for very special and rare reasons.

Extensions support
------------------

n3t Hidden Captcha uses standard Joomla! way for Captcha integration introduced
in Joomla! 1.6. In other words, it is supported by all extensions that uses this
standard Captcha implementation.

Unfortunately, until now there are lot of components and Joomla! extensions, that
rather use their own Captcha style, and do not support standard Joomla Captcha API.

With such extensions n3t Hidden Captcha will not work, and there is nothing we
could do about that. If you want to use n3t Hidden Captcha with such extension,
please contact its developer and ask him to enable standard Joomla! Captcha API
in his/her extension.