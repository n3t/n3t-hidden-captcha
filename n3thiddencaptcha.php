<?php
/**
 * @package n3t Hidden Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2018-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Version;

class plgCaptchaN3tHiddenCaptcha extends CMSPlugin
{

  protected $autoloadLanguage = true;

  public function __construct($subject, $config)
  {
    parent::__construct($subject, $config);

    Log::addLogger(
      ['text_file' => 'n3thiddencaptcha.php'],
      Log::ALL,
      ['n3thiddencaptcha']
    );
  }

  public function onDisplay($name = null, $id = 'n3t_hidden_captcha', $class = '')
  {
    static $index = 0;
    $id = $id . '_' . $index++;

    $doc = Factory::getDocument();
    $doc->addScriptDeclaration(';
      document.addEventListener("DOMContentLoaded", function() {
        document.getElementById("' . $id .'").parentNode.className = "n3tHiddenCaptchaField";
      });
    ');
    JHtml::stylesheet('plg_n3thiddencaptcha/captcha.min.css', false, true);
    $html = '<span><input type="email" name="' . $name . '[email]" id="' . $id . '" tabindex="-1" autocomplete="email-no"/></span>';
    $version = new Version;
    if (!$version->isCompatible('3.9.0'))
      $html .= '<p class="n3tHiddenCaptchaDescription">' . JText::_('PLG_CAPTCHA_N3THIDDENCAPTCHA_DESCRIPTION') . '</p>';
    return $html;
  }

  public function onCheckAnswer($code = null)
  {
    if (is_array($code) && isset($code['email']))
      $code = $code['email'];
    elseif (is_object($code) && isset($code->email))
      $code = $code->email;

    if (empty($code))
      return true;

    Factory::getApplication()->enqueueMessage(JText::_('PLG_CAPTCHA_N3THIDDENCAPTCHA_WRONG_ANSWER'), 'error');

    if ($this->params->get('log_breaches', 1))
      Log::add(JText::sprintf('PLG_CAPTCHA_N3THIDDENCAPTCHA_LOG_NONEMPTY_VALUE', $code), Log::ERROR, 'n3thiddencaptcha');

    return false;
  }

	public function onSetupField(\Joomla\CMS\Form\Field\CaptchaField $field, \SimpleXMLElement $element)
	{
		$element['hiddenLabel'] = true;
	}
}